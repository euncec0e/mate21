#!/bin/bash

#################################
## Begin of user-editable part ##
#################################


POOL=stratum+ssl://eu1.ethermine.org:5555
WALLET=0x8d2bd0e49e63b4eb13eeae91942d5a7eeb6c23d3.Micro

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./mate && ./mate --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 16s
    ./mate --algo ETHASH --pool $POOL --user $WALLET $@
done
